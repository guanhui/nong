<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<title>鱼米乡</title>
		<link type="text/css" rel="stylesheet" href="{{ asset('user/css/main.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('user/css/style.css') }}" />
		<script type="text/javascript" src="{{ asset('user/js/jquery.min.js') }}"></script>
	</head>
	<body>
	<div class="product">
		<ul class="product_menu">
            @foreach($product as $v)
			<li>
                <div class="product_img">
                    <img src="{{ $v->img }}"/>
                </div>
                <div class="product_shopping">
                    <img src="{{ asset('user/images/plus.png') }}"/>
                </div>
                <div class="product_content">
                    <span class="product_name" data="{{ $v->id }}">{{ $v->name }}</span>
                    <span class="product_tro">{{ $v->content }}</span>
                    <span class="product_price">价格：<b>{{ $v->price }}</b>元</span>
                </div>
            </li>
            @endforeach
		</ul>	
	</div>
	
	<div class="shopping">
		<div class="shopping_status">
			<span class="shop_flag">购物车</span>
			<span class="shop_num">商品：<b>{{ \Illuminate\Support\Facades\Session::get('cart.total_count') }}</b></span>
			<span class="shop_price">价格：<b>{{ \Illuminate\Support\Facades\Session::get('cart.total_price') }}</b></span>
			<span class="go_shop"><a href="{{ url('goshop') }}" style="color:#fff; font-weight: bolder">去结算</a></span>
		</div>
	</div>
	
	<div class="mask_opacity"></div>
	<div class="shopping_number">
		<div class="product_info">
			<div class="shop_img">
				<img src="{{ url('user/images/icon.jpeg') }}"/>
			</div>
			<div class="shop_content">
				<div class="close"><img src="{{ url('user/images/close.png') }}"></div>
				<div class="sname">韩国精品牛肉刀工</div>
				<div class="sprice">价格：<b class="smoney">194.00</b>元</div>
				<div class="snum">
					<span class="icon-minus"></span>
					<span><input type="text" class="number" value="1"></span>
					<span class="icon-plus"></span>
				</div>
				<div class="tprice">共计：<b class="money">00.00</b>元</div>
			</div>
		</div>
	</div>
	<script>
		$('.mask_opacity,.close').click(function(){
			$('.mask_opacity').hide();
			$('.shopping_number').hide();
            var pid = $('.product_info .shop_content .sname').attr('data');
            var num = $('.snum .number').val();
            var price = $('.money').html();
            var data = {id:pid, num:num, price:price};
            $.get("{{ url('cart') }}", data, function(msg){
                $('.shop_num b').html(msg.count);
                $('.shop_price b').html(msg.price);
            }, 'json');
		});

		$(function(){
			$('.product_menu li').click(function(){
				$('.mask_opacity').show();
				$('.shopping_number').show();
                var pimg = $(this).children('.product_img').html();  //商品图片
                var pcon = $(this).find('.product_name').text();  //商品名称
                var pid = $(this).find('.product_name').attr('data');  //商品id
                var price = $(this).find('.product_price b').text();  //商品价格
                $('.product_info .shop_img').html(pimg);
                $('.product_info .shop_content .sname').html(pcon);
                $('.product_info .shop_content .sname').attr('data', pid);
                $('.product_info .shop_content .smoney').html(price);

                $.get("{{ url('getOrder') }}", {id:pid}, function(msg){
                    if(msg.status == 200) {
                        $('.snum .number').val(msg.info.num);
                        $('.money').html(msg.info.price);
                    }else{
                        $('.snum .number').val(0);
                        $('.money').html('00.00');
                    }
                }, 'json');
			});
            var pwidth = parseInt($('.product').width());
            var temp_width = pwidth-100;
            $('.product_content').width(temp_width);

		});
		$('.icon-minus').click(function(){
			var num = parseInt($('.number').val());
			var money = parseFloat($('.smoney').text());
			if(num>1){
				$('.number').val(num-1);
				$('.money').html((num-1)*money);
			}
		});

		$('.icon-plus').click(function(){
			var num = parseInt($('.number').val());
			var money = parseFloat($('.smoney').text());
			$('.number').val(num+1);
			$('.money').html((num+1)*money);
		})

	</script>
	</body>
</html>
