<div class="footer">
    <ul>
        <li><a href="{{ url('index') }}"><span class="icon-home footer-config"></span><section>首页</section></a></li>
        <li><a href="{{ url('goshop') }}"><span class="icon-cart footer-config"></span><section>购物车</section></a></li>
        <li><a href="{{ url('shop') }}"><span class="icon-shop footer-config"></span><section>全部订单</section></a></li>
        <li><a href="{{ url('userinfo') }}"><span class="icon-user footer-config"></span><section>个人中心</section></a></li>
    </ul>
</div>