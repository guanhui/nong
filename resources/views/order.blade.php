<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<title>订单中心--鱼米香</title>
		<link type="text/css" rel="stylesheet" href="{{ url('user/css/main.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ url('user/css/style.css') }}" />
		<script src="{{ url('user/js/jquery.min.js') }}"></script>
	</head>
	<body>
		<div class="header">
			<dl>
				<dd><a href="{{ url('shop') }}" style="color: #fff">全部订单</a></dd>
				<dd><a href="{{ url('toshop') }}" style="color: #fff">进行中订单</a></dd>
			</dl>
		</div>
		
		<div class="product">
             @foreach($order as $key=>$val)
			<ul class="product_menu margin25">
				<li class="driver">
					<span>{{ $val->created_at }}</span>
					<span class="status">
                        @if($val->status == 1)
                            未派送
                        @elseif($val->status == 2)
                            派送中
                        @else
                            完成
                        @endif
                    </span>
				</li>
                @foreach($val->order as $k=>$v)
                <li>
                    <div class="product_img">
                        <img src="{{ \App\Product::find($v->product_id)->img }}"/>
                    </div>
                    <div class="product_shopping">
                        <img src="{{ asset('user/images/plus.png') }}"/>
                    </div>
                    <div class="product_content">
                        <span class="product_name" data="1">{{ \App\Product::find($v->product_id)->name }}</span>
                        <span class="product_tro">数量：<b>{{ $v->number }}</b></span>
                        <span class="product_price">价格：<b>{{ \App\Product::find($v->product_id)->price }}</b>元</span>
                    </div>
                </li>
                @endforeach
	           	<li class="total_money">
	           		总价：{{ $val->price }}元
	           	</li>
			</ul>
            @endforeach

		</div>
		@include('nav')
		<!--
		<div class="mask_opacity"></div>
		<div class="shopping_number">
			<div class="product_info">
				<div class="shop_img">
					<img src="images/icon.jpeg"/>
				</div>
				<div class="shop_content">
					<div class="close"><img src="images/close.png"></div>
					<div class="sname">韩国精品牛肉刀工</div>
					<div class="sprice">价格：<b class="smoney">194.00</b>元</div>
					<div class="snum">
						<span class="icon-minus"></span>
						<span><input type="text" class="number" value="1"></span>
						<span class="icon-plus"></span>
					</div>
					<div class="tprice">共计：<b class="money">00.00</b>元</div>
				</div>
				<div class="go_shopping">
					<input type="submit" value="购买" />
				</div>
			</div>
		</div>
		-->
		<script>
		/*$('.mask_opacity,.close').click(function(){
			$('.mask_opacity').hide();
			$('.shopping_number').hide();
		});
		
		$(function(){
			$('.product_menu li').click(function(){
				$('.mask_opacity').show();
				$('.shopping_number').show();
			});
		});
		
		$('.icon-minus').click(function(){
			var num = parseInt($('.number').val());
			var money = parseFloat($('.smoney').text());
			if(num>0){
				$('.number').val(num-1);
				$('.money').html((num-1)*money);
			}
		});
		
		$('.icon-plus').click(function(){
			var num = parseInt($('.number').val());
			var money = parseFloat($('.smoney').text());
			$('.number').val(num+1);
			$('.money').html((num+1)*money);
		})*/
	</script>
	
	</body>
</html>
