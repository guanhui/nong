<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<title>登录--鱼米香</title>
		<link type="text/css" rel="stylesheet" href="{{ asset('user/css/main.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('user/css/style.css') }}"/>
		<script src="{{ asset('user/js/jquery.min.js') }}"></script>
    </head>
	<body>
    <form action="{{ url('user/address/handle') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="header">
			<ul>
				<li>
					<span class="icon-arrow-left2"></span>
				</li>
				<span class="operator">|</span>
				<li>更改收货人</li>
				<li><button class="button-wrap">保存</button></li>
			</ul>
		</div>
		
		
		<div class="login-container">
			<div class="content-wrap" style="border-top: none;">
				<input type="text" name="getter" class="form-input" placeholder="请输入收货人姓名" />
			</div>
            <div class="content-wrap" style="border-top: none;">
                <input type="text" name="phone" class="form-input" placeholder="请输入收货人联系电话" />
            </div>
            <div class="content-wrap" style="border-top: none;">
                <input type="text" name="address" class="form-input" placeholder="请输入收货人地址" />
            </div>
		</div>
    </form>
	</body>
	<script>
		$(function(){
			var hwidth = $('.header').width();
			var width = parseInt(hwidth)-105;
			$('.header ul li:eq(1)').width(width);
		});


	</script>
</html>
