<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<title>登录--鱼米香</title>
		<link type="text/css" rel="stylesheet" href="{{ url('user/css/main.css') }}" />
	</head>
	<body>
    <form action="{{ url('user/login/handle') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="login-container">
			<div class="content-wrap">
				<input type="text" name="name" class="form-input" placeholder="请输入手机号码" />
			</div>
			<div class="content-wrap">
				<input type="password" name="password" class="form-input" placeholder="请输入密码" />
			</div>
		</div>
		
		<div class="div-container padding5">
	
			<button class="form-button">登录</button>
			
			<section><a href="#">立即注册</a></section>
	
		</div>
    </form>
	</body>
</html>
