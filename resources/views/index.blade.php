<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<title>鱼米乡</title>
		<link type="text/css" rel="stylesheet" href="{{ url('user/css/main.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ url('user/css/style.css') }}" />
        <style>
            .nav_menu li a {
                display: block;
            }
        </style>
	</head>
	<body>
	<div>
		<img src="{{ url('user/images/index.png') }}" width="100%"/>
	</div>
	
	<div class="wrapper">
		<ul class="nav_menu">
            @foreach($tag as $t)
			<li>
                <a href="{{ url('tag',[$t->id]) }}/product">
				<div><img src="{{ $t->icon }}" width="30" height="30"/></div>
				<div>{{ $t->name }}</div>
                </a>
			</li>
            @endforeach
		</ul>	
	</div>
	
	<div class="navigate-div margin25">
		<span class="font-weight">鱼米乡生活超市</span>
		<section class="instr">
			{{ $web->content }}
		</section>
	</div>
	<div class="navigate-div">
		<span class="icon-phone truck-config"></span>
		<span>联系电话</span><span class="float-right">{{ $web->phone }}</span>
	</div>
    <div class="navigate-div">
        <span class="icon-bubbles truck-config"></span>
        <span>QQ</span><span class="float-right">{{ $web->qq }}</span>
    </div>
	<div class="navigate-div" style="margin-bottom: 60px;">
		<span class="icon-truck truck-config"></span>
		<span>地址</span><span class="float-right">{{ $web->address }}</span>
	</div>

    @include('nav')

	</body>
</html>
