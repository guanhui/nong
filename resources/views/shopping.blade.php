<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<title>鱼米乡</title>
		<link type="text/css" rel="stylesheet" href="{{ asset('user/css/main.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('user/css/style.css') }}" />
		<script type="text/javascript" src="{{ asset('user/js/jquery.min.js') }}"></script>
	</head>
	<body>
    <form action="{{ url('downOrder') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="login-container">
        <div class="navigate-div">
            <b>收货信息</b>
        </div>
        <div class="content-wrap">
            <input type="text" class="form-input" name="getter" placeholder="请输入收货人姓名" value="{{ \Illuminate\Support\Facades\Session::get('userinfo')['getter'] }}" />
        </div>
        <div class="content-wrap">
            <input type="text" class="form-input" name="phone" placeholder="请输入联系电话" value="{{ \Illuminate\Support\Facades\Session::get('userinfo')['phone'] }}" />
        </div>
        <div class="content-wrap">
            <input type="text" class="form-input" name="address" placeholder="请输入收货地址" value="{{ \Illuminate\Support\Facades\Session::get('userinfo')['address'] }}"/>
        </div>
    </div>

	<div class="product margin25">
		<ul class="product_menu">
            @if($cart['product'])
                @foreach($cart['product'] as $v)
                <li>
                    <div class="product_img">
                        <img src="{{ \App\Product::find($v['id'])->img }}"/>
                    </div>
                    <div class="product_shopping">
                        <img src="{{ asset('user/images/plus.png') }}"/>
                    </div>
                    <div class="product_content">
                        <span class="product_name" data="{{ $v['id'] }}">{{ \App\Product::find($v['id'])->name }}</span>
                        <span class="product_tro">数量：<b>{{ $v['num'] }}</b></span>
                        <span class="product_price">价格：<b>{{ \App\Product::find($v['id'])->price }}</b>元</span>
                    </div>
                </li>
                @endforeach
            @else
                <div style="text-align: center; padding: 10px 0; color:#ccc"><h5>你的购物车还是空的</h5></div>
            @endif
		</ul>	
	</div>

    <div class="shopping">
        <div class="shopping_status">
            <span class="shop_flag">商品总价</span>
            <span class="shop_price"><b>{{ \Illuminate\Support\Facades\Session::get('cart.total_price') }}</b></span>
            <span class="go_shop"><button style="border: none; background: none; color: #fff; font-weight: bolder">确认</button></span>
        </div>
    </div>
    </form>
	<div class="mask_opacity"></div>
	<div class="shopping_number">
		<div class="product_info">
			<div class="shop_img">
				<img src="{{ url('user/images/icon.jpeg') }}"/>
			</div>
			<div class="shop_content">
				<div class="close"><img src="{{ url('user/images/close.png') }}"></div>
				<div class="sname">韩国精品牛肉刀工</div>
				<div class="sprice">价格：<b class="smoney">194.00</b>元</div>
				<div class="snum">
					<span class="icon-minus"></span>
					<span><input type="text" class="number" value="1"></span>
					<span class="icon-plus"></span>
				</div>
				<div class="tprice">共计：<b class="money">00.00</b>元</div>
			</div>
		</div>
	</div>
	<script>
		$('.mask_opacity,.close').click(function(){
			$('.mask_opacity').hide();
			$('.shopping_number').hide();
            var pid = $('.product_info .shop_content .sname').attr('data');
            var num = $('.snum .number').val();
            var price = $('.money').html();
            var data = {id:pid, num:num, price:price};
            $.get("{{ url('cart') }}", data, function(msg){
               // $('.shop_num b').html(msg.count);
                $('.active .product_tro b').html(msg.num);
                $('.active').removeClass('active');
                $('.shop_price b').html(msg.price);
            }, 'json');
		});

		$(function(){
			$('.product_menu li').click(function(){
				$('.mask_opacity').show();
				$('.shopping_number').show();
                $(this).addClass('active');
                var pimg = $(this).children('.product_img').html();  //商品图片
                var pcon = $(this).find('.product_name').text();  //商品名称
                var pid = $(this).find('.product_name').attr('data');  //商品id
                var price = $(this).find('.product_price b').text();  //商品价格
                $('.product_info .shop_img').html(pimg);
                $('.product_info .shop_content .sname').html(pcon);
                $('.product_info .shop_content .sname').attr('data', pid);
                $('.product_info .shop_content .smoney').html(price);
                $.get("{{ url('getOrder') }}", {id:pid}, function(msg){
                    if(msg.status == 200) {
                        $('.snum .number').val(msg.info.num);
                        $('.money').html(msg.info.price);
                    }else{
                        $('.snum .number').val(0);
                        $('.money').html('00.00');
                    }
                }, 'json');
			});
            var pwidth = parseInt($('.product').width());
            var temp_width = pwidth-100;
            $('.product_content').width(temp_width);

		});
		$('.icon-minus').click(function(){
			var num = parseInt($('.number').val());
			var money = parseFloat($('.smoney').text());
			if(num>1){
				$('.number').val(num-1);
				$('.money').html((num-1)*money);
			}
		});

		$('.icon-plus').click(function(){
			var num = parseInt($('.number').val());
			var money = parseFloat($('.smoney').text());
			$('.number').val(num+1);
			$('.money').html((num+1)*money);
		})

	</script>
	</body>
</html>
