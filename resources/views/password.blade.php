<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <title>登录--鱼米香</title>
    <link type="text/css" rel="stylesheet" href="{{ asset('user/css/main.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('user/css/style.css') }}"/>
    <script src="{{ asset('user/js/jquery.min.js') }}"></script>
</head>
<body>
<form name="frm" action="{{ url('user/password/handle') }}" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="header">
    <ul>
        <li>
            <span class="icon-arrow-left2"></span>
        </li>
        <span class="operator">|</span>
        <li>修改密码</li>
        <li><button class="button-wrap">保存</button></li>
    </ul>
</div>


<div class="login-container">
    <div class="content-wrap" style="border-top: none;">
        <input type="password" name="password_old" class="form-input" placeholder="请输入原密码" />
    </div>
    <div class="content-wrap">
        <input type="password" name="password" class="form-input" placeholder="请输入新密码" />
    </div>
    <div class="content-wrap">
        <input type="password" name="password_confirmation" class="form-input" placeholder="请输入确认密码" />
    </div>
</div>
</form>
</body>
<script>
    $(function(){
        var hwidth = $('.header').width();
        var width = parseInt(hwidth)-105;
        $('.header ul li:eq(1)').width(width);
    });

    var flag = false;
    $("input[name='password_old']").change(function(){
        var password = $(this).val();
        var data = {password:password};
        $.get("{{ url('user/password_old') }}", data, function(msg){
            if(msg.status==200){
                $("input[name='password_old']").parent('.content-wrap').css('border','none');
                $("input[name='password_old']").parent('.content-wrap').css('border-bottom','1px solid #e4e4e4');
                flag = true;
            }else{
                $("input[name='password_old']").parent('.content-wrap').css('border','1px solid #f00');
                $("input[name='password_old']").val('');
                $("input[name='password_old']").attr('placeholder', '原密码输入错误');
                flag = false;
            }
        }, 'json');
    });

    $("input[name='password']").blur(function(){
        if($(this).val().length > 5 && $(this).val().length<16){
            $(this).parent('.content-wrap').css('border','none');
            $(this).parent('.content-wrap').css('border-bottom','1px solid #e4e4e4');
            flag = true;
        }else{
            $(this).val('');
            $(this).attr('placeholder', '新密码必须是6位到16位之间');
            $(this).parent('.content-wrap').css('border','1px solid #f00');
            flag = false;
        }
    });

    $("input[name='password_confirmation']").blur(function(){
        if($(this).val()==$("input[name='password']").val()){
            $(this).parent('.content-wrap').css('border','none');
            $(this).parent('.content-wrap').css('border-bottom','1px solid #e4e4e4');
            flag = true;
        }else{
            $(this).val('');
            $(this).attr('placeholder', '确认密码和新密码不一致');
            $(this).parent('.content-wrap').css('border','1px solid #f00');
            flag = false;
        }
    });

    $('.button-wrap').click(function(){
        if(flag){
            $('form[name="frm"]').submit();
        }else{
            return false;
        }
    });
</script>
</html>
