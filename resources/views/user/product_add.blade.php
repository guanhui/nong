@extends('user/app')
@section('content')
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                      <div class="col-lg-8 col-lg-offset-2">
                          <section class="panel">
                              <header class="panel-heading">
                                  添加商品
                              </header>
                              <div class="panel-body">
                                  <form role="form" enctype="multipart/form-data"  method="post" action="{{ url('product') }}">
                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      <div class="form-group">
                                          <label for="exampleInputEmail1">商品类型</label>
                                          <select class="form-control" name="type_id">
                                              @foreach($tag as $v)
                                                <option value="{{ $v->id }}">{{ $v->name }}</option>
                                              @endforeach
                                          </select>
                                      </div>
                                      <div class="form-group">
                                          <label for="exampleInputEmail1">商品名称</label>
                                          <input type="text" name="name" value="" class="form-control" placeholder="商品名称">
                                      </div>

                                      <div class="form-group">
                                          <label for="exampleInputEmail1">商品价格</label>
                                          <input type="text" name="price" value="" class="form-control" placeholder="商品价格">
                                      </div>

                                      <div class="form-group">
                                          <label for="exampleInputFile">商品图片</label>
                                          <input type="file" name="img" placeholder="图标">
                                      </div>
                                      <div class="form-group">
                                          <label for="exampleInputFile">商品描述</label>
                                          <textarea name="content" class="form-control" style="min-height: 100px"></textarea>
                                      </div>
                                      <div class="form-group">
                                          <label for="exampleInputPassword1">排序</label>
                                          <input type="text" name="sort" value="0" class="form-control" placeholder="排序">
                                      </div>
                                      <button type="submit" class="btn btn-info">添加</button>
                                  </form>

                              </div>
                          </section>
                      </div>
                    </div><!--row1-->
                </section><!-- /.content -->
                <link rel="stylesheet" type="text/css" href="{{ asset('/simditor/styles/simditor.css') }}" />
                <script type="text/javascript" src="{{ asset('/simditor/scripts/jquery.min.js') }}"></script>
                <script type="text/javascript" src="{{ asset('/simditor/scripts/module.js') }}"></script>
                <script type="text/javascript" src="{{ asset('/simditor/scripts/hotkeys.js') }}"></script>
                <script type="text/javascript" src="{{ asset('/simditor/scripts/uploader.js') }}"></script>
                <script type="text/javascript" src="{{ asset('/simditor/scripts/simditor.js') }}"></script>
                <script>
                    var editor = new Simditor({
                        textarea: $('#editor'),
                        upload: true
                    });
                </script>
@endsection