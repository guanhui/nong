@extends('user/app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-lg-6 col-lg-offset-3">
              <section class="panel">
                  <header class="panel-heading">
                      网站信息
                  </header>
                  <div class="panel-body">
                      <form role="form" method="post" action="{{ url('web', [$web->id]) }}">
                          <input type="hidden" name="_method" value="PUT">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <div class="form-group">
                              <label for="exampleInputEmail1">联系方式</label>
                              <input type="text" name="phone" value="{{ $web->phone }}" class="form-control" placeholder="Enter email">
                          </div>
                          <div class="form-group">
                              <label for="exampleInputPassword1">QQ</label>
                              <input type="text" name="qq" value="{{ $web->qq }}" class="form-control" placeholder="Password">
                          </div>
                          <div class="form-group">
                              <label for="exampleInputPassword1">地址</label>
                              <input type="text" name="address" value="{{ $web->address }}" class="form-control" placeholder="Password">
                          </div>
                          <div class="form-group">
                              <label for="exampleInputPassword1">简介</label>
                              <textarea name="content" class="form-control" style="min-height: 100px">{{ $web->content }}</textarea>
                          </div>
                          <button type="submit" class="btn btn-info">修改</button>
                      </form>

                  </div>
              </section>
          </div>
        </div><!--row1-->
    </section><!-- /.content -->
@endsection