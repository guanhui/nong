@extends('user/app')
@section('content')
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                      <div class="col-lg-6 col-lg-offset-3">
                          <section class="panel">
                              <header class="panel-heading">
                                  添加商品分类
                              </header>
                              <div class="panel-body">
                                  <form role="form" enctype="multipart/form-data"  method="post" action="{{ url('tag', [$tag->id]) }}">
                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      <input type="hidden" name="_method" value="PUT">
                                      <div class="form-group">
                                          <label for="exampleInputEmail1">分类名称</label>
                                          <input type="text" name="name" value="{{ $tag->name }}" class="form-control" placeholder="分类名称">
                                      </div>
                                      <div class="form-group">
                                          <label for="exampleInputFile">图标</label>
                                          <input type="file" name="icon" placeholder="图标">
                                      </div>
                                      <div class="form-group">
                                          <label for="exampleInputPassword1">排序</label>
                                          <input type="text" name="sort" value="{{ $tag->sort }}" class="form-control" placeholder="排序">
                                      </div>
                                      <button type="submit" class="btn btn-info">修改</button>
                                  </form>

                              </div>
                          </section>
                      </div>
                    </div><!--row1-->

                </section><!-- /.content -->
@endsection