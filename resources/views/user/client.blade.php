@extends('user/app')
@section('content')
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel">
                                <header class="panel-heading">
                                    客户管理

                                </header>
                                <!-- <div class="box-header"> -->
                                    <!-- <h3 class="box-title">Responsive Hover Table</h3> -->

                                <!-- </div> -->
                                <div class="panel-body table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>姓名</th>
                                            <th>性别</th>
                                            <th>收件人</th>
                                            <th>联系电话</th>
                                            <th>地址</th>
                                        </tr>
                                        @foreach($client as $val)
                                        <tr>
                                            <td>{{ $val->name }}</td>
                                            <td>
                                                @if($val->sex == 2)
                                                    女
                                                @elseif($val->sex == 1)
                                                    男
                                                @else
                                                    保密
                                                @endif
                                            </td>
                                            <td>{{ $val->getter }}</td>
                                            <td>{{ $val->phone }}</td>
                                            <td>{{ $val->address }}</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                    <?php echo $client->render();?>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
@endsection