@extends('user/app')
@section('content')
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel">
                                <header class="panel-heading">
                                    商品管理
                                <span style="float: right;"><a href="{{ url('product/create') }}">添加</a></span>
                                </header>
                                <!-- <div class="box-header"> -->
                                    <!-- <h3 class="box-title">Responsive Hover Table</h3> -->

                                <!-- </div> -->
                                <div class="panel-body table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>编号</th>
                                            <th>名称</th>
                                            <th>类型</th>
                                            <th>排序</th>
                                            <th>操作</th>
                                        </tr>
                                        @foreach($product as $val)
                                        <tr>
                                            <td>{{ $val->id }}</td>
                                            <td>{{ str_limit($val->name, 15) }}</td>
                                            <td>
                                                @foreach($val->tags as $t)
                                                    {{ $t->name }}
                                                @endforeach
                                            </td>
                                            <td>{{ $val->sort }}</td>
                                            <td>
                                                <a href="{{ url('product',[$val->id]) }}/edit">修改</a>
                                                <form action="{{ url('product', [$val->id]) }}" method="post">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="submit" style="background: none; border: none; position: absolute; margin: -20px 0 0 30px;" value="删除">
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>
                                    <?php echo $product->render();?>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
@endsection