@extends('user/app')
@section('content')
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                      <div class="col-lg-6 col-lg-offset-3">
                          <section class="panel">
                              <header class="panel-heading">
                                  个人信息
                              </header>
                              <div class="panel-body">
                                  <form role="form" method="post" action="{{ url('setting/handle') }}">
                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      <div class="form-group">
                                          <label for="exampleInputEmail1">用户名</label>
                                          <input type="text" name="name" value="{{ \Illuminate\Support\Facades\Auth::user()->name }}" class="form-control" id="exampleInputEmail1" placeholder="用户名">
                                      </div>
                                      <div class="form-group">
                                          <label for="exampleInputEmail1">邮箱</label>
                                          <input type="email" name="email" value="{{ \Illuminate\Support\Facades\Auth::user()->email }}" class="form-control" id="exampleInputEmail1" placeholder="邮箱">
                                      </div>
                                      <div class="form-group">
                                          <label for="exampleInputPassword1">原密码</label>
                                          <input type="password" name="password_old" class="form-control" placeholder="原密码">
                                      </div>
                                      <div class="form-group">
                                          <label for="exampleInputPassword1">新密码</label>
                                          <input type="password" name="password" class="form-control" placeholder="新密码">
                                      </div>
                                      <div class="form-group">
                                          <label for="exampleInputPassword1">确认密码</label>
                                          <input type="password" name="password_confirmed" class="form-control" placeholder="确认密码">
                                      </div>
                                      <button type="submit" class="btn btn-info">修改</button>
                                  </form>

                              </div>
                          </section>
                      </div>
                    </div><!--row1-->

                </section><!-- /.content -->
@endsection