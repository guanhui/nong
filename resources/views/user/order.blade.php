@extends('user/app')
@section('content')
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel">
                                <header class="panel-heading">
                                    订单管理

                                </header>
                                <!-- <div class="box-header"> -->
                                    <!-- <h3 class="box-title">Responsive Hover Table</h3> -->

                                <!-- </div> -->
                                <div class="panel-body table-responsive">
                                    <table class="table table-hover">
                                        @foreach($order as $key=>$val)
                                        <tr>
                                            <td><b style="@if($val->flag != 0)color: #cc3333 @else color: #ccc @endif">订单编号：{{ $val->order_number }}</b></td>
                                            <td></td>
                                            <td style="text-align: right">
                                                @if($val->flag !=0)
                                                    @if($val->status == 1)
                                                        <a href="{{ url('order/send', [$val->id,$val->status]) }}">
                                                            未派送
                                                        </a>
                                                    @elseif($val->status == 2)
                                                        <a href="{{ url('order/send', [$val->id,$val->status]) }}">
                                                            派送
                                                        </a>
                                                    @else
                                                        完成
                                                    @endif
                                                @endif
                                            </td>
                                            <td style="text-align: right">
                                                @if($val->flag != 0)
                                                <a href="{{ url('order/cancel', [$val->id]) }}">取消订单</a>
                                                @else
                                                <font color="#ccc">订单已取消</font>
                                                @endif
                                            </td>
                                            <td style="width: auto">{{ $val->created_at }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <table class="table table-hover" style="margin-bottom: 0;">
                                                    @foreach($val->order as $v)
                                                    <tr>
                                                        <td><img src="{{ \App\Product::find($v->product_id)->img  }}" width="30" height="30"></td>
                                                        <td>{{ \App\Product::find($v->product_id)->name  }}</td>
                                                        <td>{{ $v->number }}</td>
                                                        <td>{{ \App\Product::find($v->product_id)->price  }}元</td>
                                                    </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ $val->getter }}</td>
                                            <td style="text-align: right">{{ $val->phone }}</td>
                                            <td>{{ $val->address }}</td>
                                            <td></td>
                                            <td>商品总价：<b style="color: #cc3333">{{ $val->price }}</b>元<span style="padding-left: 10px"></span>数量x{{ $val->order()->count() }}</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                    <?php echo $order->render();?>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
@endsection