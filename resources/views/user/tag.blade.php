@extends('user/app')
@section('content')
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel">
                                <header class="panel-heading">
                                    商品分类
                                <span style="float: right;"><a href="{{ url('tag/create') }}">添加</a></span>
                                </header>
                                <!-- <div class="box-header"> -->
                                    <!-- <h3 class="box-title">Responsive Hover Table</h3> -->

                                <!-- </div> -->
                                <div class="panel-body table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>编号</th>
                                            <th>名称</th>
                                            <th>排序</th>
                                            <th>图标</th>
                                            <th>操作</th>
                                        </tr>
                                        @foreach($tag as $val)
                                        <tr>
                                            <td>{{ $val->id }}</td>
                                            <td>{{ $val->name }}</td>
                                            <td>{{ $val->sort }}</td>
                                            <td>{{ $val->icon }}</td>
                                            <td>
                                                <a href="{{ url('tag',[$val->id]) }}/edit">修改</a>
                                                <form action="{{ url('tag', [$val->id]) }}" method="post">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="submit" style="background: none; border: none; position: absolute; margin: -20px 0 0 30px;" value="删除">
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>
                                    <?php echo $tag->render();?>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
@endsection