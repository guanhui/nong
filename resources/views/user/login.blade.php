<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>鱼米乡--登录</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="{{ asset('nong/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{ asset('nong/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{ asset('nong/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="{{ asset('nong/css/style.css') }}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <div class="wrapper row-offcanvas row-offcanvas-left">

            <aside class="right-side">
                <!-- Content Header (Page header) -->


                <!-- Main content -->
                <section class="content">
                    <div class="row">
                      <div class="col-lg-6 col-lg-offset-2" style="padding-top: 60px;">
                          <section class="panel">
                              <header class="panel-heading">
                                  登录
                              </header>
                              <div class="panel-body">
                                  <form class="form-horizontal" role="form" method="post" action="{{ url('login/handle') }}">
                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      <div class="form-group">
                                          <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">邮箱</label>
                                          <div class="col-lg-10">
                                              <input type="email" name="email" class="form-control" id="inputEmail1" placeholder="邮箱">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <label for="inputPassword1" class="col-lg-2 col-sm-2 control-label">密码</label>
                                          <div class="col-lg-10">
                                              <input type="password" name="password" class="form-control" id="inputPassword1" placeholder="密码">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <div class="col-lg-offset-2 col-lg-10">
                                              <div class="checkbox">
                                                  <label>
                                                      <input type="checkbox" name="remember"> 记住密码
                                                  </label>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <div class="col-lg-offset-2 col-lg-10">
                                              <button type="submit" class="btn btn-danger">登录</button>
                                          </div>
                                      </div>
                                  </form>
                              </div>
                          </section>
                      </div>
                    </div><!--row1-->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <script src="{{ asset('nong/js/jquery.min.js') }}" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="{{ asset('nong/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <!-- Director App -->
        <script src="{{ asset('nong/js/Director/app.js') }}" type="text/javascript"></script>
    </body>
</html>
