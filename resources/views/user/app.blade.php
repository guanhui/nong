<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>鱼米香</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="description" content="Developed By M Abdur Rokib Promy">
    <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
    <!-- bootstrap 3.0.2 -->
    <link href="{{ asset('nong/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="{{ asset('nong/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset('nong/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- google font -->
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <!-- Theme style -->
    <link href="{{ asset('nong/css/style.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-black">
<!-- header logo: style can be found in header.less -->
<header class="header">
    <a href="{{ url('/') }}" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        {{ \Illuminate\Support\Facades\Auth::user()->name }}
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-right">
            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user"></i>
                        <span>{{ \Illuminate\Support\Facades\Auth::user()->email }} <i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                        <li class="dropdown-header text-center">账号信息</li>
                        <!--
                        <li>
                            <a href="#">
                                <i class="fa fa-clock-o fa-fw pull-right"></i>
                                <span class="badge badge-success pull-right">10</span> Updates</a>
                            <a href="#">
                                <i class="fa fa-envelope-o fa-fw pull-right"></i>
                                <span class="badge badge-danger pull-right">5</span> Messages</a>
                            <a href="#"><i class="fa fa-magnet fa-fw pull-right"></i>
                                <span class="badge badge-info pull-right">3</span> Subscriptions</a>
                            <a href="#"><i class="fa fa-question fa-fw pull-right"></i> <span class=
                                                                                              "badge pull-right">11</span> FAQ</a>
                        </li>

                        <li class="divider"></li>
                        -->
                        <li>
                            <a href="{{ url('web') }}">
                                <i class="fa fa-user fa-fw pull-right"></i>
                                网站信息
                            </a>
                            <a data-toggle="modal" href="{{ url('setting') }}">
                                <i class="fa fa-cog fa-fw pull-right"></i>
                                修改密码
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="{{ url('logout') }}"><i class="fa fa-ban fa-fw pull-right"></i> 退出</a>
                        </li>
                    </ul>

                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ asset('nong/img/avatar3.png') }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>你好, {{ \Illuminate\Support\Facades\Auth::user()->name }}</p>

                    <a href="#"><i class="fa fa-circle text-success"></i> 在线</a>
                </div>
            </div>

            <ul class="sidebar-menu">
                <li>
                    <a href="{{ url('client') }}">
                        <i class="fa fa-dashboard"></i> <span>客户管理</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('tag') }}">
                        <i class="fa fa-gavel"></i> <span>商品类型</span>
                    </a>
                </li>

                <li>
                    <a href="{{ url('product') }}">
                        <i class="fa fa-globe"></i> <span>商品管理</span>
                    </a>
                </li>

                <li>
                    <a href="{{ url('order') }}">
                        <i class="fa fa-glass"></i> <span>订单管理</span>
                    </a>
                </li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">

        @yield('content')

        <div class="footer-main">
            Copyright &copy 鱼米香, 2015
        </div>
    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->


<!-- jQuery 2.0.2 -->
<script src="{{ asset('nong/js/jquery.min.js') }}" type="text/javascript"></script>

<!-- Bootstrap -->
<script src="{{ asset('nong/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- Director App -->
<script src="{{ asset('nong/js/Director/app.js') }}" type="text/javascript"></script>
</body>
</html>
