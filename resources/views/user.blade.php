<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>用户中心--鱼米香</title>
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<link type="text/css" rel="stylesheet" href="{{ url('user/css/main.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ url('user/css/style.css') }}"/>
        <style>
            .navigate-div a {
                display: block;
            }
        </style>
	</head>
	<body>
		<div class="navigate-div margin25">
			<a href="{{ url('user/password') }}">
                <span>
                    <img src="{{ url('user/images/icon.png') }}" width="30" height="30">
                </span>
                <span class="tel">13526071455</span>
            </a>
		</div>
		
		<div class="navigate-div margin25">
            <a href="{{ url('shop') }}">
			<span class="icon-cart truck-config"></span>我的订单
            </a>
		</div>
		
		<div class="navigate-div margin25">
            <a href="{{ url('user/address') }}">
			    <span class="icon-earth truck-config"></span>默认收货地址
            </a>
		</div>
		<div class="navigate-div">
            <a href="{{ url('user/address') }}">
			<span class="icon-user truck-config"></span>
			<span>收件人</span><span class="float-right">{{ \Illuminate\Support\Facades\Session::get('userinfo')['getter'] }}</span>
		    </a>
        </div>
		<div class="navigate-div">
            <a href="{{ url('user/address') }}">
			<span class="icon-phone truck-config"></span>
			<span>联系电话</span><span class="float-right">{{ \Illuminate\Support\Facades\Session::get('userinfo')['phone'] }}</span>
            </a>
        </div>
		<div class="navigate-div">
            <a href="{{ url('user/address') }}">
			<span class="icon-truck truck-config"></span>
			<span>送货地址</span><span class="float-right">{{ \Illuminate\Support\Facades\Session::get('userinfo')['address'] }}</span>
		    </a>
        </div>
		
		<div class="navigate-div margin25 user-btn">
			<a href="{{ url('logout') }}"><button class="form-button">退出登录</button></a>
		</div>
        @include('nav')
	</body>
</html>
