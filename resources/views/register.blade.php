<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<title>注册--鱼米香</title>
		<link type="text/css" rel="stylesheet" href="css/main.css" />
	</head>
	<body>
    <form action="{{ url('user/register/handle') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="login-container">
			<div class="content-wrap">
				<input type="text" name="name" class="form-input" placeholder="请输入用户名" />
			</div>
			<div class="content-wrap">
				<input type="password" name="password" class="form-input" placeholder="请输入密码" />
			</div>
			<div class="content-wrap">
				<input type="password" name="password_confirmation" class="form-input" placeholder="请输入确认密码" />
			</div>
		</div>
		
		<div class="div-container padding5">
			<button class="form-button">立即注册</button>
			
			<section>
				<a href="#">立即登录</a>
			</section>
		</div>
    </form>
	</body>
</html>
