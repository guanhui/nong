<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	//

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'product_tags', 'product_id', 'tag_id');
    }

    public function orders()
    {
        return $this->belongsToMany('App\Product', 'order_products', 'order_id', 'product_id');
    }

}
