<?php namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {

    /**
     * 登录
     */
	public function login()
    {
        return view('login');
    }

    /**
     * 登录处理
     */
    public function loginHandle(Request $request)
    {
        $clients = Client::where('name', '=', $request->input('name'))->get();
        foreach($clients as $key=>$val)
        {
            if(Hash::check($request->input('password'), $val->password)) {
                Session::put('userinfo', $val);
                return Redirect::to('index');
            }
        }
        return redirect()->to('user/login');
    }

    /**
     * 注册
     */
    public function register()
    {
        return view('register');
    }

    /**
     * 注册处理
     */
    public function registerHandle(Request $request)
    {
        $validator = Validator::make($request->all(), Client::$rules);
        if($validator->fails())
        {
            $client = new Client();
            $client->name = $request->input('name');
            $client->password = Hash::make($request->input('password'));
            $client->save();
            return Redirect::to('user/login');
        }
        else
        {
            return redirect()->back();
        }
    }

    /**
     * 注销
     */
    public function logout()
    {
        Session::flush();
        return Redirect::to('user/login');
    }

    /**
     * 用户中心
     */
    public function user()
    {
        return view('user');
    }

    /**
     * 修改密码
     */
    public function password()
    {
        return view('password');
    }

    /**
     * 修改密码
     */
    public function passwordHandle(Request $request)
    {
        $client = Client::find(Session::get('userinfo')['id']);
        $client->password = Hash::make($request->input('password'));
        $client->save();
        return Redirect::to('userinfo');
    }

    /**
     * 匹配原密码是否正确
     */
    public function CheckPassword(Request $request)
    {
        if(Hash::check($request->input('password'), Session::get('userinfo')['password']))
        {
            $msg = [
                'status' => 200,
                'msg' => '正确'
            ];
        }
        else
        {
            $msg = [
                'status' => 404,
                'msg' => '错误'
            ];
        }
        return json_encode($msg);
    }

    /**
     * 修改收货地址
     */
    public function address(){
        return view('address');
    }

    public function addressHandle(Request $request)
    {
        $client = Client::find(Session::get('userinfo')['id']);
        $client->getter = $request->input('getter');
        $client->phone = $request->input('phone');
        $client->address = $request->input('address');
        $client->save();
        Session::put('userinfo', $client);
        return Redirect::to('userinfo');
    }
}
