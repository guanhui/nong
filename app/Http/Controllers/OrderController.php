<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Order;
use App\OrderProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Monolog\Handler\NewRelicHandlerTest;

class OrderController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $order = Order::where('status', '!=', 0)->orderBy('created_at', 'desc')->get();

		return view('order')->with('order', $order);
	}

    //进行中订单
    public function goshop()
    {
        $order = Order::where('status', '=', 1)->orderBy('created_at', 'desc')->get();
        return view('good')->with('order', $order);
    }
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    /**
     * 购物车
     * Session['cart']['total_num']   //商品总数量
     * Session['cart']['total_count']   //商品种类
     * Session['cart']['total_price']   //商品总价格
     * Session['cart']['prodcut']  //商品
     */
    public function cart(Request $request)
    {
        $id = $request->input('id');
        $num = intval($request->input('num'));
        $price = floatval($request->input('price'));

        if(!$id){
            return json_encode(array('status'=>404));
        }

        $temp = [
            'id' => $id,
            'num' => $num,
            'price' => $price
        ];

        $temp_session = Session::get('cart.product');
        $product = [];
        if($temp_session)
        {
            //判断客户是否已经购买过本商品
            $flag = false;
            $total_num = 0;  //商品总数量
            $total_price = 0;
            foreach($temp_session as $key=>$val)
            {
                if($val['id']==$id)
                {
                    $val['num'] = $num;
                    $val['price'] = $price;
                    $flag = true;
                }
                $product[] = $val;
                $total_num += $val['num'];
                $total_price += $val['price'];
            }

            if(!$flag){
                $product[] = $temp;
                $total_num += $num;
                $total_price += $price;
            }
            Session::put('cart.total_num', $total_num);
            Session::put('cart.total_count', count(Session::get('cart.product')));
            Session::put('cart.total_price', $total_price);

        }
        else
        {
            $product[0] = $temp;
            Session::put('cart.total_num', $num);
            Session::put('cart.total_count', 1);
            Session::put('cart.total_price', $price);
        }
        Session::put('cart.product', $product);
        $msg = [
            'status' => 200,
            'message' => '成功',
            'num' => $num,
            'count' =>  Session::get('cart.total_count'),
            'price' =>  Session::get('cart.total_price')
        ];
        return json_encode($msg);
    }

    /**
     * 获取商品在购物车中的信息
     * @param Request $request
     */
    public function getCart(Request $request)
    {
        $id = $request->input('id');
        $temp = Session::get('cart.product');
        $msg = [];
        if($temp )
        {
            foreach($temp as $key=>$val)
            {
                if($val['id']==$id)
                {
                    $msg = [
                        'status' => 200,
                        'message' => '存在',
                        'info' => [
                            'num' => $val['num'],
                            'price' => $val['price']
                        ]
                    ];
                }
            }
        }

        if(!$msg)
        {
            $msg = [
                'status' => 404,
                'message' => '不存在'
            ];
        }

        return json_encode($msg);
    }

    /**
     * 购物车列表
     */
    public function cartList()
    {
        $cart = Session::get('cart');
        return view('shopping')->with('cart', $cart);
    }

    /**
     * 下单购物
     */
    public function downOrder(Request $request)
    {
        $order = new Order();
        $order->order_number = date('Ymd').mt_rand(0,4);
        $order->getter = $request->input('getter');
        $order->phone = $request->input('phone');
        $order->address = $request->input('address');
        $order->price = Session::get('cart.total_price');
        $order->save();

        foreach(Session::get('cart.product') as $val)
        {
            $op = new OrderProduct();
            $op->order_id = $order->id;
            $op->product_id = $val['id'];
            $op->number = $val['num'];
            $op->save();
        }

        Session::pull('cart');
        return Redirect::to('index');
    }

}
