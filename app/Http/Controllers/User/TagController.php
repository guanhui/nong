<?php namespace App\Http\Controllers\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Tag;
use Illuminate\Support\Facades\Redirect;

class TagController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tag = Tag::paginate(10);
        return view('user.tag')->with('tag', $tag);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('user.tag_add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $tag = new Tag();
        $tag->name = $request->input('name');
        $tag->sort = $request->input('sort');

        if($request->hasFile('icon') && $request->file('icon')->isValid())
        {
            $upload = $request->file('icon');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).".".$fileinfo['extension'];
            $upload->move('upload', $filename);
            $tag->icon = "http://".$_SERVER['SERVER_NAME']."/upload/".$filename;
        }
        $tag->save();
        return Redirect::to('tag');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$tag = Tag::find($id);
        return view('user.tag_edit')->with('tag', $tag);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$tag = Tag::find($id);
        $tag->name = $request->input('name');
        $tag->sort = $request->input('sort');
        if($request->hasFile('icon') && $request->file('icon')->isValid())
        {
            $upload = $request->file('icon');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).".".$fileinfo['extension'];
            $upload->move('upload', $filename);
            $tag = "http://".$_SERVER['REMOTE_HOST']."/upload/".$filename;
        }
        $tag->save();
        return Redirect::to('tag');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Tag::find($id)->delete();
        return Redirect::to('tag');
	}

}
