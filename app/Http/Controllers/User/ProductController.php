<?php namespace App\Http\Controllers\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Tag;

class ProductController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $product = Product::orderBy('created_at', 'desc')->paginate(10);
        return view('user.product')->with('product', $product);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $tag = Tag::all();
		return view('user.product_add')->with('tag', $tag);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$product = new Product();
        $product->name = $request->input('name');
        $product->content = $request->input('content');
        $product->sort = $request->input('sort');
        $product->price = $request->input('price');
        if($request->hasFile('img') && $request->file('img')->isValid())
        {
            $upload = $request->file('img');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).".".$fileinfo['extension'];
            $upload->move('upload/product', $filename);
            $product->img = "http://".$_SERVER['SERVER_NAME']."/upload/product/".$filename;
        }
        $product->save();
        $product->tags()->attach($request->input('type_id'));
        return Redirect::to('product');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $tag = Tag::all();
		$product = Product::find($id);
        return view('user.product_edit')->with('product', $product)->with('tag', $tag);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
        $product = Product::find($id);
        $product->name = $request->input('name');
        $product->content = $request->input('content');
        $product->sort = $request->input('sort');
        $product->price = $request->input('price');
        if($request->hasFile('img') && $request->file('img')->isValid())
        {
            $upload = $request->file('img');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).".".$fileinfo['extension'];
            $upload->move('upload/product', $filename);
            $product->img = "http://".$_SERVER['SERVER_NAME']."/upload/product/".$filename;
        }
        $product->save();
        return Redirect::to('product');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        Product::find($id)->tags()->detach();
		Product::find($id)->delete();
        return Redirect::to('product');
	}

}
