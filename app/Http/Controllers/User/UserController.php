<?php namespace App\Http\Controllers\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {

    /**
     * 登录界面
     * @return \Illuminate\View\View
     */
	public function login()
    {
        return view('user.login');
    }

    /**
     * 登录处理
     */
    public function loginHandle(Request $request)
    {
        if(Auth::attempt(['email'=>$request->input('email'), 'password'=>$request->input('password')]))
        {
            return Redirect::to('web');
        }
        else
        {
            return redirect()->back();
        }
    }

    /**
     * 显示个人信息（页面）
     */
    public function setting()
    {
        return view('user.setting');
    }

    /**
     * 修改个人信息（页面）
     */
    public function settingHandle(Request $request)
    {
        if((Hash::check($request->input('password_old'), Auth::user()->password)))
        {
            return redirect()->back();
        }

        $user = User::find(Auth::user()->id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');

        if($request->input('password') == $request->input('password_confirmed') && $request->input('password') )
        {
            $user->password = Hash::make($request->input('password'));
        }

        $user->save();

        return redirect()->back();
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('login');
    }
}
