<?php namespace App\Http\Controllers\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Web;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class WebController extends Controller {

	/**
     * 网站信息修改
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$web = Web::find(1);
        return view('user.web')->with('web', $web);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$web = Web::find($id);
        $web->qq = $request->input('qq');
        $web->phone = $request->input('phone');
        $web->address = $request->input('address');
        $web->content = $request->input('content');
        $web->save();
        return Redirect::to('web');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
