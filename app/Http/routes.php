<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'IndexController@index');

Route::get('home', 'HomeController@index');

Route::get('login', 'User\UserController@login');

Route::post('login/handle', 'User\UserController@loginHandle');

Route::get('logout', 'User\UserController@logout');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['middleware'=>'auth', 'namespace'=>'User'], function(){
    Route::resource('web', 'WebController');   //网站信息设置
    Route::get('setting', 'UserController@setting');   //修改账号密码
    Route::post('setting/handle', 'UserController@settingHandle');
    Route::resource('client', 'ClientController');  //客户管理
    Route::resource('tag', 'TagController');  //商品类型
    Route::resource('product', 'ProductController');   //商品
    Route::resource('order', 'OrderController');   //商品
    Route::get('order/cancel/{id}', 'OrderController@orderCancel');
    Route::get('order/send/{id}/{status}', 'OrderController@orderSend');
});


Route::get('user/login', 'UserController@login');
Route::get('user/register', 'UserController@register');
Route::post('user/login/handle', 'UserController@loginHandle');
Route::post('user/register/handle', 'UserController@registerHandle');

Route::group(['middleware'=>'check'],function(){
    Route::resource('index', 'IndexController');
    Route::get('logout', 'UserController@logout');
    Route::get('userinfo', 'UserController@user');
    Route::get('user/password', 'UserController@password');  //修改密码
    Route::post('user/password/handle', 'UserController@passwordHandle');  //修改密码处理
    Route::get('user/password_old', 'UserController@CheckPassword');
    Route::get('user/address', 'UserController@address');
    Route::post('user/address/handle', 'UserController@addressHandle');  //修改收货地址
    Route::resource('tag.product', 'ProductController');
    Route::resource('shop', 'OrderController');  //订单
    Route::get('cart', 'OrderController@cart');  //购物车
    Route::get('getOrder', 'OrderController@getCart');  //购物车中商品信息
    Route::get('goshop', 'OrderController@cartList');  //购物车中商品信息
    Route::post('downOrder', 'OrderController@downOrder');  //购物车中商品信息
    Route::get('toshop', 'OrderController@goshop');  //购物车
});
