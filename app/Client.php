<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model {

    public $fillables = ['name', 'password'];

    public static $rules = [
        'name' => 'alpha|between:6,16',
        'password' => 'required|alpha_num|between:6,12|confirmed',
        'password_confirmation' => 'required|alpha_num|between:6,12',
    ];
}
