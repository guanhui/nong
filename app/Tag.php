<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

    public function prodcuts()
    {
        return $this->belongsToMany('App\Product', 'product_tags', 'tag_id', 'product_id');
    }
}
