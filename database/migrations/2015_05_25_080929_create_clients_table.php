<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clients', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name', 20)->nullable();
            $table->string('open_id')->nullable();
            $table->string('password', 60)->nullable();
            $table->tinyInteger('sex')->default(2);
            $table->string('getter',20)->nullable();
            $table->string('phone',15)->nullable();
            $table->string('address')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clients');
	}

}
