<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('webs', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('qq',20)->nullabe();
            $table->string('phone',15)->nullabe();
            $table->string('address')->nullabe();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('webs');
	}

}
