<?php

use Illuminate\Database\Seeder;
use App\Web;

class WebTableSeeder extends Seeder{

    public function run()
    {
        DB::table('webs')->delete();
        Web::create([
            'qq' => '123456',
            'phone' => '123456789',
            'address' => '鱼米香',
            'content' => '超级市场，常简称超市，是指以顾客自选方式经营食品、家庭日用品、食物为主的大型综合性零售商场。是以顾客自选方式经营的大型综合性零售商场，又称自选商场，是许多国家特别是经济发达国家的主要商业零售组织形式。'
        ]);
    }
}