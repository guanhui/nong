<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserTableSeeder extends Seeder{

    public function run()
    {
        DB::table('users')->delete();
        User::create([
            'name' => '鱼米香',
            'email' => 'yumixiang@yu.com',
            'password' => Hash::make('yumixiang')
        ]);
    }
}